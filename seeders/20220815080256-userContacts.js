'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
      await queryInterface.bulkInsert('UserContacts',[{
        UserId:1,
        contactName:"SanthoshFather",
        contactNumber:"9989614256",
        contactEmail:"santhoshFather@gmail.com",
        createdAt:new Date(),
        updatedAt:new Date(),

    },{
      UserId:1,
      contactName:"SanthoshMother",
      contactNumber:"9989614257",
      contactEmail:"santhoshMother@gmail.com",
      createdAt:new Date(),
      updatedAt:new Date(),
    }])

    
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('UserContacts', null, {});
  }
};
