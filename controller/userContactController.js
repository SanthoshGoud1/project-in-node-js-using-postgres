const { includes } = require('lodash');
var Model = require('../models');
//getting All Users Data
module.exports.getContactDetails = (req, res)=> {
      Model.Users.findAll({include:Model.UserContact})
        .then((user)=> {
            res.send(user);
        })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
};