var Model = require('../models');
//getting All Users Data
module.exports.getAll = (req, res)=> {
      Model.Users.findAll()
        .then((user)=> {
            res.send(user);
        })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
};



//getting Only Singe user Data by using id
   exports.getOne= (req, res) => {
        const id = req.params.id;
        console.log(id);
        Model.Users.findByPk(id)
          .then(data => {
            if (data) {
              res.send(data);
            } else {
              res.status(404).send({
                message: `Cannot find Tutorial with id=${id}.`
              });
            }
          })
          .catch(err => {
            res.status(500).send({
              message: "Error retrieving Tutorial with id=" + id
            });
          });
      };



//create an new user 

module.exports.create = function (req, res) {
  
        // Validate request
        if (!req.body.firstName) {
          res.status(400).send({
            message: "Content can not be empty!"
          });
          return;
        }
        // Create a user
        const user = {
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          createdAt:new Date(),
          UpdatedAt:new Date()
        };
        // Save users in the database
        Model.Users.create(user)
          .then(data => {
            res.send(data);
          })
          .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating the Tutorial."
            });
          });
      };


//deleting user using id
exports.delete = (req, res) => {
    const id = req.params.id;
    Model.Users.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Tutorial was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Tutorial with id=" + id
        });
      });
  };
   
//UserContact contact details