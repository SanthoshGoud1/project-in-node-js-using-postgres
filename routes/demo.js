const userContoller=require("../controller/userControllers");
const userContactController=require("../controller/userContactController")
var express = require('express');
const { param } = require(".");
var router = express.Router();
var Model = require('../models');

router.get('/',userContoller.getAll);
router.get('/getOne/:id',userContoller.getOne)
router.post('/create',userContoller.create)
router.delete('/delete/:id',userContoller.delete)

//userContact Details Router
router.get('/userContacts',userContactController.getContactDetails)


module.exports=router;
  